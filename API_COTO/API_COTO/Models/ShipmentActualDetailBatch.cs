﻿using System;
using System.Collections.Generic;

namespace API_COTO.Models
{
    public partial class ShipmentActualDetailBatch
    {
        public int Id { get; set; }
        public int ShipmentActualDetailId { get; set; }
        public int? ProductId { get; set; }
        public string LotNumber { get; set; }
        public int? Quantity { get; set; }
        public decimal? QuantityToStore { get; set; }
        public DateTimeOffset? ProductionDate { get; set; }
        public decimal? ExpiredDate { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDelete { get; set; }
        public DateTimeOffset? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public DateTimeOffset? ModifyDate { get; set; }
        public int? ModifyBy { get; set; }
        public DateTimeOffset? ProductionDateAccounting { get; set; }
        public DateTimeOffset? ExpiredDateAccounting { get; set; }
        public bool? IsImportFromAccounting { get; set; }
        public int? StoreIdAccounting { get; set; }
        public string NoteAccounting { get; set; }
        public decimal? BeginPeriodAccounting { get; set; }
        public decimal? ImportQuantityAccounting { get; set; }
        public decimal? ExportQuantityAccounting { get; set; }
        public decimal? EndPeriodAccounting { get; set; }
    }
}
