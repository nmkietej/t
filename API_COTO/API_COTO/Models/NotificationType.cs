﻿using System;
using System.Collections.Generic;

namespace API_COTO.Models
{
    public partial class NotificationType
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Template { get; set; }
        public string Description { get; set; }
        public DateTimeOffset? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDelete { get; set; }
        public int? SortOrder { get; set; }
        public string SearchString { get; set; }
    }
}
