﻿using System;
using System.Collections.Generic;

namespace API_COTO.Models
{
    public partial class ItemgroupItems
    {
        public int Id { get; set; }
        public int? ItemId { get; set; }
        public int? ItemGroupId { get; set; }
        public string Notes { get; set; }
        public bool? IsDelete { get; set; }
        public DateTimeOffset? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
    }
}
