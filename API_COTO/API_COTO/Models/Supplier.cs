﻿using System;
using System.Collections.Generic;

namespace API_COTO.Models
{
    public partial class Supplier
    {
        public int SupplierId { get; set; }
        public string SupplierCode { get; set; }
        public string SupplierName { get; set; }
        public string AbbreviatedName { get; set; }
        public string SupplierAddress { get; set; }
        public string CountryId { get; set; }
        public string Remark { get; set; }
        public string PaymentTerm { get; set; }
        public bool? IsActive { get; set; }
        public string SearchString { get; set; }
        public bool? IsDelete { get; set; }
        public int? CreatedBy { get; set; }
        public DateTimeOffset? CreatedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTimeOffset? ModifiedDate { get; set; }
        public int? SortOrder { get; set; }
    }
}
