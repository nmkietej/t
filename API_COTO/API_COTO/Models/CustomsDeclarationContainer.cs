﻿using System;
using System.Collections.Generic;

namespace API_COTO.Models
{
    public partial class CustomsDeclarationContainer
    {
        public int Id { get; set; }
        public int? CustomsDeclarationId { get; set; }
        public int? ContainerType { get; set; }
        public double? Quantity { get; set; }
        public string ContainerCode { get; set; }
        public string Note { get; set; }
        public DateTimeOffset? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public int? ModifyBy { get; set; }
        public DateTimeOffset? ModifyDate { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDelete { get; set; }
    }
}
