﻿using System;
using System.Collections.Generic;

namespace API_COTO.Models
{
    public partial class RolePermissionClone
    {
        public int RolePermissionId { get; set; }
        public int RoleId { get; set; }
        public int PermissionId { get; set; }
    }
}
