﻿using System;
using System.Collections.Generic;

namespace API_COTO.Models
{
    public partial class CustomsDeclaration
    {
        public int Id { get; set; }
        public string DeclarationCode { get; set; }
        public string CustomsName { get; set; }
        public DateTimeOffset? DeclarationDate { get; set; }
        public string ShipmentBy { get; set; }
        public int? InventoryId { get; set; }
        public int? HarborId { get; set; }
        public string Currency { get; set; }
        public double? ExchangeRate { get; set; }
        public string ContainerNumber { get; set; }
        public int? ContainerType { get; set; }
        public string TkhqNo { get; set; }
        public DateTimeOffset? TkhqDate { get; set; }
        public DateTimeOffset? CustomsClearanceDate { get; set; }
        public DateTimeOffset? Etd { get; set; }
        public DateTimeOffset? Eta { get; set; }
        public DateTimeOffset? ReceivedDate { get; set; }
        public DateTimeOffset? InputDate { get; set; }
        public string CertificateNumber { get; set; }
        public DateTimeOffset? CertificateDate { get; set; }
        public decimal? InsuranceFee { get; set; }
        public decimal? TransportFee { get; set; }
        public decimal? BoatFee { get; set; }
        public decimal? SpecializedFee { get; set; }
        public decimal? FreightDownFee { get; set; }
        public decimal? CustomsFee { get; set; }
        public decimal? HarborFee { get; set; }
        public decimal? OtherFee { get; set; }
        public decimal? ImportTaxTotal { get; set; }
        public decimal? VatTotal { get; set; }
        public decimal? Total { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDelete { get; set; }
        public DateTimeOffset? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public DateTimeOffset? ModifyDate { get; set; }
        public int? ModifyBy { get; set; }
        public string Note { get; set; }
        public string SearchString { get; set; }
        public int? SortOrder { get; set; }
        public string CoThirdNumber { get; set; }
        public string CoNumber { get; set; }
        public DateTimeOffset? CoDate { get; set; }
        public string CoAdjust { get; set; }
        public string ImportCerNo { get; set; }
        public DateTimeOffset? ImportCerDate { get; set; }
        public string Blnumber { get; set; }
        public DateTimeOffset? Bldate { get; set; }
    }
}
