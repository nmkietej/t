﻿using System;
using System.Collections.Generic;

namespace API_COTO.Models
{
    public partial class InternalOrderDetail
    {
        public int Id { get; set; }
        public int? InternalOrderId { get; set; }
        public int? ProductId { get; set; }
        public int? ProductPackageUnitId { get; set; }
        public int? QuantityRequest { get; set; }
        public DateTimeOffset? RequestEta { get; set; }
        public DateTimeOffset? BookedEta { get; set; }
        public DateTimeOffset? DateClearance { get; set; }
        public DateTimeOffset? DateToStore { get; set; }
        public string Note { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDelete { get; set; }
        public DateTimeOffset? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public DateTimeOffset? ModifyDate { get; set; }
        public int? ModifyBy { get; set; }
    }
}
